$(document).ready(function () {

    $("#p-1 img").on("click", function () {
        $("#pantograph-front").toggle();
    });

    $("#p-4 img").on("click", function () {
        $("#pantograph-back").toggle();
    });

    (function poll() {
        setTimeout(function () {
            $.ajax({
                //url: "http://localhost:4434/pantograph/lastUpdate",
				url: "http://localhost:4434/pantograph/lastUpdate",
                type: "GET",
                success: function (data) {
                    console.log(data)
                    //$("#p-2").html(data.id); //return when data is parsed successfully
                    //$("#p-3").html(data.predictions.tagName); //return when data is parsed successfully
                    //$("#thickness").html($('<h3>').text(""+data.thickness+" mm"));

                    if (data != null) {

                        $("#train-id h1").html("RABE-2-" + data.id);

                        //---------------------------- FRONT ---------------------------

                        var abplatzung = "";
                        if(data.first.abplatzung) {
                            abplatzung = "Ja";
                        } else {
                            abplatzung = "Nein";
                        }

                        var diagonalCut = true;
                        if(data.first.diagonalCut) {
                            diagonalCut = "Ja";
                        } else {
                            diagonalCut = "Nein";
                        }
                        $("#thickness").html(data.first.thickness + " mm");
                        $("#abplatzung").html(abplatzung);
                        $("#created").html(data.first.created);
                        $("#diagonal-cut").html(diagonalCut);
                        $("#predictions").html(data.first.predictedMaintenanceDate);

                        var color = "";
                            if (data.first.thickness < 45) {
                                color=  "red";
                            } else if (data.first.abplatzung == true || data.first.diagonalCut == true) {
                                color= "yellow";
                            } else {
                                color= "green";
                            }
                        console.log(color)
                        $("#p-1 img").attr("src", "media/train/left-" +
                            color + ".png");
                        $("#pantograph-image-left").attr("src", "Hackathon/" + data.id + "/FirstBrushOK1L.png");
                        $("#pantograph-image-right").attr("src", "Hackathon/" + data.id + "/FirstBrushOK1R.png");

                        //---------------------------- BACK-----------------------------
                        
                        var abplatzung = "";
                        if(data.second.abplatzung) {
                            abplatzung2 = "Ja";
                        } else {
                           abplatzung2 = "Nein";
                        }

                        var diagonalCut2 = "";
                        if(data.second.diagonalCut) {
                            diagonalCut2 = "Ja";
                        } else {
                            diagonalCut2 = "Nein";
                        }
                        
                        $("#thickness2").html(data.second.thickness + " mm");
                        $("#abplatzung2").html(abplatzung2);
                        $("#created2").html(data.second.created);
                        $("#diagonal-cut2").html(diagonalCut2);
                        $("#predictions2").html(data.second.predictedMaintenanceDate);



                        var color2 = "";
                            if (data.second.thickness < 45) {
                                color2=  "red";
                            } else if (data.second.abplatzung == true || data.second.diagonalCut == true) {
                                color2= "yellow";
                            } else {
                                color2= "green";
                            }
                        console.log(color2)
                        $("#p-4 img").attr("src", "media/train/right-" +
                            color2 + ".png");
                        $("#pantograph-image-left2").attr("src", "Hackathon/" + data.id + "/SecondBrushOK1L.png");
                        $("#pantograph-image-right2").attr("src", "Hackathon/" + data.id + "/SecondBrushOK1R.png");
                    }

                },
                error: function (err) { console.log(err) },
                dataType: "json",
                complete: poll,
                timeout: 10000
            })
        }, 1000);


    })();
});



